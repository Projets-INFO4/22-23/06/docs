![Polytech](https://www.polytech-grenoble.fr/medias/photo/grenoble-inp-polytech-grenoble-blanc_1643365295850-png)

# Cannonball project

[Logbook](CannonBall_info4_2022_2023.md)

## Description

As part of our semester project (S7), we have to take over the "ConnonBall" project. We will use GNSS RTK modules with centimeter precision to guide a KYOSHO Scorpion XXL vehicle.

![KYOSHO Scorpion XXL](https://ae01.alicdn.com/kf/S2c9ee2ea4d0b43f989015cd4efd22ed6t.jpg_640x640Q90.jpg_.webp)

## Geolocation

This consists of using a network called centipede to geolocate using GNSS and RTK technologies.

![Centipede](https://d1fmx1rbmqrxrr.cloudfront.net/zdnet/i/edit/ne/2021/09/RTK.jpg)

## Visual recognition

This consists of using an application called Yolo to recognize objects via a visual data stream (video, images, etc.)

![YOLO](https://miro.medium.com/max/700/0*xfXdebLeaMXt3Vct)
