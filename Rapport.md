---
header-includes:
    - \usepackage{fullpage}
---

# Project Cannonball Report

## Part 1: Background

In the framework of our semester project (S7), we have to resume the "ConnonBall" project. We will use RTK GNSS modules with centimeter accuracy to guide a KYOSHO Scorpion XXL vehicle.

### Presentation of the team

Clément Galinier : Project Manager.

THOMAZO Corentin : Scrum Master.

CORREIA DE OLIVEIRA Diego

GALLIER Loric

### Objective

The team's goal is to get as far as possible in these steps:

- Make the car work.
- Make the car work via a program on an RPI.
- Make the car work via a program on an RPI.
- Make the car work via a centimetric gps.
- Make the car work via a program and gps coordinates.
- Make the car work via a program and a predefined path of gps coordinates.
- Make the RPI recognize obstacles via a camera.
- Make the car avoid obstacles.
- To make the car can record the courses on pro circuits, and learn to repeat them.

### Functional description of the requirements

**Functional specification**

All the functions to make it work :

- Drive the car via a program in a RPI connected to it.
- The car follows a gps tracker
- the car can learn trajectory.
- the car avoids obstacles thanks to the camera.
- the camera recognizes the obstacles.

**Non-functional specification**

Non-functional requirements are about performance, availability and scalability. They define how your application should perform under various circumstances, such as load or error, and how it will scale with different levels of usage.

- If the signal is lost, the car stops.
- In case of problem, the user can take control with the remote control.

## Part 2: Organization

![Workflow](Workflow.png)

Concerning the planning, it has been disrupted many times, either by technical problems (RPI not working because of a bug on the new version of the RPI bone), logistic (the fablab on strike) or simply the lack of experience in project management that is inevitable (due to the fact that we rarely have projects of this size).

For the organization of the work we used the peer programming that we saw in the Software Engineering course. This method, added to the fact that we discussed the code regularly, is interesting because everyone has an overview of the code in general. Moreover, since we worked with technologies that we did not master at the beginning of the project, the peer programming allowed us not to waste too much time on a lack of knowledge of the technology used. Indeed, we had to start the project with a big and long documentation work, and then we worked together so that each one brings the knowledge linked to the documentation he had made.

## Part 3: Technical details

### About the vehicle

Here is the way you are suppose to connect the Arduino the the vehicle :

![schema](arduino_car.png)

### GNSS RTK

GNSS RTK stands for Global Navigation Satellite System Real-Time Kinematic. It is an accurate positioning technology used in a variety of applications including mapping, surveying, construction and surveying.

GNSS is a satellite-based positioning system that uses signals from satellites orbiting the Earth to determine the position of a GNSS receiver on the Earth's surface. RTK is a method of processing these signals to provide highly accurate position measurements in real time.

GNSS RTK works by using a GNSS RTK receiver and an antenna that receives signals from GNSS satellites in real time. The GNSS RTK receiver processes these signals to calculate the exact position of the receiver to within a few millimeters.

RTK processing uses a method called real-time kinematics (RTK), which uses additional signals from a base station at a known, stable location to correct errors in the GNSS signal. The base station calculates these corrections and sends them to the GNSS RTK receiver in real time, thus greatly improving positioning accuracy.

In summary, GNSS RTK is a real-time accurate positioning technology that uses GNSS signals and an RTK processing method to provide very precise positioning measurements, with an accuracy of up to a few millimeters.

**Installation**

First we update and configure the GNSS RTK receiver with Ucenter. We also need to open the serial ports of the raspberry pi 3 .Then we install the gpsd daemon to retrieve the coordinates. Finally we install an ntripclient which is an external code.
For more details on the installation see [GNSS_install](GNSS_receiver.md).

**Limit**

The refresh rate is relatively slow to give the GNSS RTK receiver time to correct the coordinates.

### YOLO

**Description**

Yolo is an artificial intelligence model enabling element recognition on picture and video. As an exemple it can detect people on a webcam.

**Installation**

Yolo version 5 that we are using is available at this [link](https://github.com/ultralytics/yolov5). \
To install Yolov5 clone the [git repository](git@github.com:ultralytics/yolov5.git), the installation is finished by running the `pip install -r requirements.txt` command in the cloned repository. Be careful the python modules installation may need up to 5 GB depending on already installed modules.

**Limit**

Yolo.v5 need 5GB of spaces and also a great computing power to reduce image treatment latency.

**Issues**

The computing power and the size of the image to be processed are the main factors of processing speed. In the context of this project, we need a very high reactivity. But considering a small image (640x480 pixels), a gtx 1650 need 50ms to process the image. As we can't get a Nvidia Jetson or an equivalent card, we have a Raspberry Pi 3 and an Arduino Uno card that hasn't the computing power needed.

## Part 4: What we did

Here is the list and explanations of everything we did:

### RPI

The RPI is the system coordinator. It controls the car by giving motor commands to the Arduino to follow a predefined track, and to do so it uses an RTK GNSS localization system.

To make the RPI the system coordinator, we developed the following features:

The RPI can gather the RTK coordination
The RPI drive the car by giving motor command through the serial port
The RPI can parse CSV files that represent shapes those shapes can be
A track that represents a path to follow
A safe zone which defines as a shape where the car must stay
The RPI follows the predefined track using a naive method, if the car is aligned to the next point then the car goes straight forward else it turns to the left until the car is aligned to the next point
For security reasons :
If the car goes out of the safe zone, the RPI stops the car
If the car is moving faster than a predefined value, the RPI stops the car
If the car doesn't receive any coordinate during a defined amount of time, the RPI stops the car

### Arduino

The Arduino processes and transmits the RPI orders to the car's servo motor. The Arduino also received speed and angle targets from the RPI serial port.

The Arduino stores its current speed and angle and smooths the transition to reach the targeted ones. If the Arduino receives an emergency message from the RPI, it will stop the car as soon as possible.

### GPS

First of all, we found out what GNSS RTK is. How to use GNSS RTK and collect the necessary streams.
We decided to use the nmea stream to send the coordinates of the GNSS RTK receiver to the raspberry pi 3. To make the RTK base talk to the rover we use an NTRIP stream.

Then we received the hardware to start doing the necessary installations and software configuration.

Then we received the hardware to start doing the necessary installations and software configuration. You can find all these manipulations on the git doc with the file name: GNSS_receiver.md.
This step took a lot of time as we encountered several problems with the raspberry pi 3 and its AMA0 serial port.

Then we were able to make some code to check that the GPS coordinates arrived well and was coherent for that we coded two small programs sender.py and receiver.py. sender.py recovered the coordinates thanks to the GPS module which used the gpsd daemon then sent them to receiver.py by means of a tail, which displayed them on the terminal.

Finally we were able to integrate the gps code into main.py by using the sender.py as a second process and adding functionality to the get_coord function.

### RPI -> Arduino

In order to send motor commands to the Arduino, we defined the following message types:

| type | arg | example | description |
| :- | :- | :- | :- |
| "e" | None | "e" | Emergency message to stop the car and not restart the car until the system is reset |
| "s" | `String(number)[3]` | "s100" | Send a speed request to the Arduino |
| "a" | `String(number)[3]` | "s100" | Send a angle request to the Arduino |

### Arduino -> Vehicle

To control the car using the Arduino, we plugged in pins 9 and 10 of the Arduino to control respectively the angle and the speed of the car. The Arduino sends a value between 0 and 180 to the servo motor with the following effect:

| Value  | effect                     |
| :----- | :------------------------- |
| 0-90   | speed up (90 max speed)    |
| 91     | stop the car               |
| 92-180 | go backward (92 max speed) |

### GPS -> RPI

## Part 5: What remains to be done

Unfortunately we didn't have time to do everything in the time we had, so here is what is left to do:

### Testing

Most of the tests have been done, the general integration test remains, all the unit tests have been done, but not formalized due to lack of time.

### YOLO

Concerning the YOLO part, it has only been documented, because we didn't have the necessary computing power to really look at it (we made it work on the PC and we already had some latency). On the other hand we have documented everything we did on it (see Part 3: YOLO).

### Arduino security

In case the RPI crashes (a Python program error or freeze), the car needs to be stopped. To do so, the Arduino should stop the car if it doesn't receive any data for a predefined amount of time. That implies editing the RPI Python code to periodically send the command (even if it is the same one).

## Part 6: Conclusion

### Our difficulties

Our organization may not have been perfect, but we don't think that's the core of the problem. The fact is that we are in a computer science field and therefore we have little practical electronic knowledge. The project was therefore complicated to get used to and the documentation was long. In the same way, we were confronted with a multitude of small problems which, by following one another, made us lose time. For example, we lost 30 minutes just because we didn't connect the ground on the card of the car.

### What we learned

We learned what it's like to work on a big project, with all the organization aspect that it implies, unlike last year's project which was much faster. We were able to learn about the basics of electronics, how a centimeter gps works via GNSS RTK, as well as programming on RPI and Arduino. This will allow us to be a little more helpful if we are confronted with a project of this kind in the future.
