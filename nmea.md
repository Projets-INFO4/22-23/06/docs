# NMEA (National Marine Electronics Association)

NMEA is an international standard for communication between marines electronics and GPS receivers.

## Message structure

The NMEA use a specific structure, that can be found on [Wikipedia](https://fr.wikipedia.org/wiki/NMEA_0183). Those data can be parsed quite easily using python.

## Python library

A python library named `nmea` is available but it still in beta. Which is interesting to us because we plan to use python.

The library can be installed via this command : `pip install nmea`

### Init error fix

First installing the module there are some error while initializing the module. The error mentioned a module named SocketServer, that isn't found. To fix it, the line mentioning `import SocketServer` in the server.py can be replace by `import socketserver as SocketServer`.

In fact, python2 use SocketServer and python3 use socketserver.
