# Specifications

## Context

### Summary

As part of our semester project (S7), we have to take over the "ConnonBall" project. We will use GNSS RTK modules with centimeter precision to guide a KYOSHO Scorpion XXL vehicle.

### Team presentation

Clément Galinier : Project Manager.

THOMAZO Corentin : Scrum Master.

CORREIA DE OLIVEIRA Diego

GALLIER Loric

## Goals

The team's goal is to go as far as possible in these steps:

- Run the little car.
- Run the little car through a program on an RPI.
- Operate the car via program and gps coordinates.
- Operate the car via a program and a predefined course of gps coordinates.
- Make the RPI recognize obstacles via a camera.
- Make the car able to avoid obstacles.
- Make sure the car can record courses on pro circuits, and learn to repeat them.

## Functional description of needs

### Functional specifications

All the functions to make it work.

- Drive the car via a program in an RPI connected to it.
- The car follows a GPS track.
- The car can learn trajectories.
- The camera recognizes obstacles.
- The car avoids obstacles thanks to the camera.

### Non-functional specification

Non-functional requirements relate to performance, availability, and scalability. They define how your app should perform under various circumstances, including under load or error, and how it will scale with varying levels of usage.

- In case of loss of signal, the car stops.
- In the event of a problem, the user can regain control with the remote control.

## Resources

cf : inventory.

## Deadline

We have until ...
