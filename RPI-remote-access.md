---
title: RPI remote access
header-includes:
    - \usepackage{fullpage}
---

## Problem

As the RPI will be on the car, we needed a solution to run the code while being on the track. So we managed to find a solution that is a remote access using SSH.

## Installation

On the RPI run the following command

```bash
raspi-config # enable ssh
sudo apt install tightvncserver
```

## Usage

Run the following command on your pc to connect remotely

```bash
ssh user@ipaddress tightvncserver # To start vnc server
remmina -c vnc://user:password@address:port # To remote access the RPI
```
