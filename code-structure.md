---
title: code-structure
header-includes:
    - \usepackage{fullpage}
---

## General idea

The system use a RPI connected to an Arduino Uno using the serial port. The serial port is used to command the Arduino Uno (accelerate, turn, etc) using the GNSS RTK information received and transmitted to the RPI.

## Arduino

The main features the Arduino Uno must implements :

- Parse the serial port information
- Command the servo motor according to the RPI order
- Stop the car if no information are received

Here is a little pseudo-code to implement the system :

```c
// Initialize variable :
currentAngle
currentSpeed

targetedAngle
targetedSpeed

lastAngleActualizationTime
lastSpeedActualizationTime
lastInstructionActualizationTime

stopNow

Loop:
    Receive RPI order
    if (new order is emergency (Stop)) {
        lastAngleActualizationTime = 0
        lastSpeedActualizationTime = 0
    }

    if lastAngleUpdateIsOlderThan50ms {
        updateAngle()
    }

    if lastSpeedUpdateIsOlderThan50ms {
        updateSpeed()
    }
```

## RPI

Général module of the Python code :

- safezone
- track
  - checkpoint
- distance
- RTK
  - Features :
    - own main function
    - stream coord and time
- Car
  - core of the program
  - position management
  - speed management
    - limitation
    - control
  - orientation management
  - path correction

## TODO

- Arduino code
- Test pipe line (share data between processes)
- Test Arduino on car
- Write the RPI python classes
- Test safezone
