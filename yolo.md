# Yolo documentation

## Description

Yolo is an artificial intelligence model enabling element recognition on picture and video. As an exemple it can detect people on a webcam.

## Installation

Yolo version 5 that we are using is available at this [link](https://github.com/ultralytics/yolov5). \
To install Yolov5 clone the [git repository](git@github.com:ultralytics/yolov5.git), the installation is finished by running the `pip install -r requirements.txt` command in the cloned repository. Be careful the python modules installation may need up to 5 GB depending on already installed modules.

## Limit

Yolov5 need 5GB of spaces and also a great computing power to reduce image treatment latency.

## Issues

The computing power and the size of the image to be processed are the main factors of processing speed. In the context of this project, we need a very high reactivity. But considering a small image (640x480 pixels), a gtx 1650 need 50ms to process the image. As we can't get a Nvidia Jetson or an equivalent card, we have a Raspberry Pi 3 and an Arduino Uno card that hasn't the computing power needed.
