# Documentation Yolo

## Description

Yolo est un modèle d'intelligence artificielle permettant la reconnaissance d'élément dans une image ou un flux vidéo. Yolo permet par exemple de détecter des personnes sur une caméra.

## Installation

La version 5 de Yolo que nous utilisons est disponible via ce [lien](https://github.com/ultralytics/yolov5). \
Après avoir cloné le [dépôt git](git@github.com:ultralytics/yolov5.git), l'installation des modules python se fait en lançant la commande `pip install -r requirements.txt` dans le dépôt cloné. Attention un espace de 5 Go peut être demandé suivant les modules déjà installés sur votre machine.

## Limitation

En plus des 5 Go d'espace de stockage nécessaire, Yolo requiert une grande puissance de calcul pour réduire la latence entre chaque traitement d'image.

## Problèmes rencontrés

La puissance de calcul et la taille de l'image à traiter sont les principaux facteurs de vitesse de traitement. Dans le contexte de ce projet, nous avons besoin d'une très grande réactivité. Or en considérant une image de petite taille (640x480 pixels), une gtx 1650 a besoin de 50ms pour traiter l'image. Ne pouvant pas nous procurer de Nvidia Jetson ou de carte équivalente, nous disposons seulement d'un Raspberry Pi 3 et d'une carte Arduino Uno qui n'ont pas la puissance de calcul nécessaire.
