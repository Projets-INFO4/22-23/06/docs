# Cahier des charges

## Contexte

### Résumé
Dans le cadre de notre projet semestriel (S7), nous devons reprendre la projet "ConnonBall". Nous allons utiliser des modules GNSS RTK a précision centimétrique pour guider un véhicule KYOSHO Scorpion XXL.

### Présentation de l'équipe

Clément Galinier : Chef de Projet.

THOMAZO Corentin : Scrum Master.

CORREIA DE OLIVEIRA Diego

GALLIER Loric

...

## Objectif

L'objectif de l'équipe est d'aller le plus loin possible dans ces étapes :
* Faire fonctionner la voiture.
* Faire fonctionner la voiture via un programme sur une RPI.
* Faire fonctionner la voiture via un programme et des coordonnées gps.
* Faire fonctionner la voiture via un programme et un parcours prédéfini de coordonnées gps.
* Faire en sorte que le RPI reconnaisse les obstacles via une caméra.
* Faire en sorte que la voiture puisse eviter les obstacles.
* Faire en sorte que la voiture puisse enregister les parcours sur circuits de pro, et apprendre pour les répéters.

## Description fonctionnelle des besoins

### Spécification fonctionnelles

Toute les fonctions pour que cela marche.

- Piloter la voiture via un programme dans un RPI connecté a cette dernière.

- La voiture suit un tracer gps

- la voiture peut apprendre des trajectoire.

- la voiture evite les obstacles grace a la camera.

- la caméra reconnait les obstacles.

### Spécification non fonctionnelles

Les exigences non fonctionnelles concernent les performances, la disponibilité et l’évolutivité. Elles définissent la manière dont votre application doit fonctionner dans diverses circonstances, notamment en cas de charge ou d’erreur, ainsi que la manière dont elle évoluera en fonction des différents niveaux d’utilisation.

- En cas de perte de signal, la voiture s'arrete.

- En cas de problème l'utilisateur peut reprendre la main avec la télécommande.

## Ressources

cf : inventaire

## Délais

Nous avons jusqu'au ... pour avancer le projet.