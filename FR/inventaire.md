# Inventaire

## Materiel informatique

- Alimentation USB 5V pour raspberry pi.
- Raspberry pi 3 model B. (fonctionne avec ubuntuMATE)
- Adaptateur microSD/SD.
- Une carte microSD 32Gb. (fonctionne avec une partition boot pour le pi)
- Un emetteur ultrason custom.
- 3 recepteur ultrason (fonctionnement douteux), avec une carte custom 
- Camera logitech (en état de fonctionnement)
- Camera microsoft (fonctionne pas).
- Carte arduino
- Adapteur Tangus (4 ports USB).
- u-blox Antenne GNSS Multi-bandes.
- GNSS RTK Click.

## Materiel Vehicule

- 2 Vehicules Scorpio. (Elles fonctionnent).
- 2 Batteries (Les deux fonctionnent).
- 2 Telecommandes (lié au scorpio).
- 1 Borne de charge avec cable d'alimentation, le detrompeur et l'équilibreur de cellule (c'est un câble).
