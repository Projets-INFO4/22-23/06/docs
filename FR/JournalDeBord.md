# Journal de bord

## Première séance (16/01/2023) :

Nous avons fait l'inventaire complet de ce que nous avions a disposition initialement, en vérifiant également l'état de fonctionnement de tout ce dont nous avons fait l'inventaire. Constatant que les batteries fonctionnent toujours contrairement a ce que l'on aurai cru, mais ayant également des difficultés a faire fonctionner une des deux voitures.

## Deuxième séance (23/01/2023) :

Nous avons réussi a faire fonctionner la deuxième voiture. Nous avons choisi les langages que nous favoriserons, a savoir principalement Pyhton pour sa légèreté et C. Nous commencons a voir un problème poindre a l'horizon : nous penssons que nous allons devoir limité la vitesse de la voiture car le programme risque de ne pas pouvoi suivre la vitesse. Nous avons également fini le cahier des charges et le trefilio. Nous avons commender a regarder la camera et YOLO qui est un logiciel permettant la reconnaissance d'objet via caméra. Enfin nous avons commencer a regarder comment faire la transition digital <-> analogique pour faire le lien entre le RPI et la voiture.

## Troisième séance (30/01/2023) :

Depuis la séance dernière nous nous sommes heurtés à un certain nombre de problèmes. Tout d’abord le Nvidia Jetson est trop volumineux pour la voiture et donc inutilisable dans le cadre de notre projet. Également le voltage de la voiture est de 6V, celui du RPI est de 5V et il n’existe pas de convertisseur 6V->5V fiable. RTKlib est de base prévu pour window, nous cherchons encore à le faire fonctionner sous linux. De plus, YOLO (le logiciel de reconnaissance par image) semble de plus en plus compliqué a déployé sur un RPI, d’autant plus sans le Jetson. Mais il y a également de bonnes nouvelles, on a réussi à faire communiquer le RPI et l'arduino via le port série, ce qui nous rend optimiste quant au fait de pouvoir piloter le véhicule via du code. Nous avons également commencé à regarder pour faire des impressions 3D de “chassi” pour maintenir les composants dans la voiture et si possible à l'abri (en cas d’accidents).

## Quatrième séance (06/02/2023):

Nous avons fais beaucoup de recherche de Documentation en rapport aux problèmes rencontrés. nous avons aussi réfléchi a la structure du code que nous allons faire en fonction des technologie que l'on a choisi.

## Cinquième séance (20/02/2023)

Cette séance, avec le retour de vacance, nous avons décider de nous reconcentré sur le coeur de projet, c'est a dire le guidage centimètre d'une voiture autonome. Ainsi pendant que nous réfléchissions aux aspects importants a présenté pour la présentation de mi-projet, nous avons commencer a coder la réupération des coordonnées via l'antenne pour la voiture ainsi que des mouvement de base qu'execute la voiture afin d'avoir des base pour la suite du projet. Nous avons heurté un problème, c'est le voltage, le RPI n'est pas assez puissant (3V) pour envoyer des signaux sur toute la plage de la voiture (6V).

## Sixième séance (27/02/2023)

Cette séance nous avons continué la préparation de la présentation de mi-projet. Nous avons également réussie a faire fonctionné l'antenne gps, nous permettant de récupéré nos coordonnée. D'autre part, nous continuons le code coté voiture en réfléchissant a utiliser un Arduino pour compenser le manque de voltage.