# GNSS receiver


How to install and configure a receiver for 

## 1) necessary material
- GNSS receiver (GNSS RTK CLIK)
- ublox antenna
- a metal plate of 15 cm by 15 cm
- Rasberry on rasbian OS

### Optionnaly
- a micro USB cable
- a computer on windows OS

## 2)Configure a GNSS receiver 
To configure the GNSS-RTK receiver we will use the manufacturer's software Ucenter 

Download the latest firmware version:
https://content.u-blox.com/sites/default/files/2022-05/UBX_F9_100_HPG132.df73486d99374142f3aabf79b7178f48.bin

Connect the receiver to the PC with the micro USB cable
Click on the icon on the top left and then on COM-.
Click on tools then firmware
Select the file to be downloaded
Check that the baud rate is 9600
And finally Go.

Download the config.txt file in this git
This config file is configuration for the receiver

To add it:
Select tools then Receiver Configuration
Select the config.twt file
then click on transfer file --> file

If you want to modify this configuration go to view then configuration view.

Now you can check the correct operation on the PC by connecting to the NTRIP client
To do this click on receiver then NTRIP client. Fill in the values in the fields. 



To open the pin serial port:

sudo raspi-config
then
1. - 5 Interfacing Options
1. - P6 Serial.
1. Select NO to Would you like a login console...
1. Select YES to Would you like the serial port hardware ...
1. - Ok
1. - Finish
1. - Reboot - Yes.

It is possible that the UART0 port is not enabled on the serial port
For this you will have to modify the rasberry pi config file:
vi /boot/config.txt
``` enable_uart=1```
``` dtoverlay=desable-bt```
``` dtoverlay=minihuart-bt```
 


## 3)Install a deamon for coordinate
Install gpsd
    sudo apt install gpsd
    
then configure the file of demaon

vi /etc/default/gpsd

```START_DAEMON="true"```
```USBAUTO="true"```
```DEVICE="/dev/ttyAMA0"```
```GPSD_OPTIONS="-n"```

### Optionally
You can test the installation with a client

Install client 
```sudo apt install gpsd-clients```
Then 
```xpgs```

You should have an interface with GPS coordinates but they are not yet RTK because there is no NTRIP stream sent to the RTK GNSS receiver.

## 4)Install a NTRIP client

We use an existing github project with MIT licence
https://github.com/nunojpg/ntripclient

clone this project on rasberry pi 

On the folder ntripclient 
``` make clean ```
then 
``` make ```
Now you can try to launch the program with this command:

```./ntripclient -s hostname -m mountpoint -D serial port -B baudrate```

For example:

```./ntripclient -s caster.centipede.com -m CRO2 -D /dev/ttyAMA0 -B 9600```
                    
## 5) Testing

You can test the values by programming a small program in python using the gps module. Like the program receiver.py and sender.py in our git code.