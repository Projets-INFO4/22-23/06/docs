# Inventory

## common

- USB power supply for raspberry pi.
- Raspberry pi 3 model B. (work with ubuntuMATE)
- Adapter microSD/SD.
- A 32Gb microSD card. (works with a boot partition for the RPI)
- A custom ultrasonic transmitter.
- 3 ultrasonic receiver (doubtful operation), with a custom card
- Logitech camera (en état de fonctionnement)
- Microsoft camera (fonctionne pas).
- Arduino card.
- Adapter Tangus (4 ports USB).
- u-blox Multi-band GNSS Antenna.
- GNSS RTK Click.

## About the car

- 2 Scorpio cars. (working).
- 2 batteries (working).
- 2 Remotes (with scorpio).
- 1 Charging terminal with power cable, polarizer and cell balancer (it's a cable).
