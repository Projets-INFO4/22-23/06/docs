# Logbook

## First session (16/01/2023) :

We took a complete inventory of what we initially had available, also checking the working condition of everything we took inventory of. Noting that the batteries still work contrary to what we would have thought, but also having difficulty operating one of the two cars.

## Second session (23/01/2023) :

We managed to get the second car working. We have chosen the languages ​​that we will favor, namely mainly Pyhton for its lightness and C. We are starting to see a problem looming on the horizon: we think that we are going to have to limit the speed of the car because the program may not be able to follow the speed. We have also finished the specifications and the trefilio. We started looking at the camera and YOLO which is a software allowing object recognition via camera. Finally we started to look at how to make the digital <-> analog transition to make the link between the RPI and the car.

## Third session (30/01/2023) :

Since the last session we have encountered some problems. First of all, the Nvidia Jetson is too bulky for the car and therefore unusable for our project. Also the voltage of the car is 6V, and the RPI is 5V and there is no reliable 6V->5V converter. RTKlib is originally intended for window, we are still trying to make it work under linux. Additionally, YOLO (image recognition software) seems increasingly complicated deployed on an RPI, even more so without the Jetson. But there is also good news, we managed to get the RPI and the arduino to communicate via the serial port, which makes us optimistic about being able to drive the vehicle via code. We also started looking to make 3D prints of “chassis” to keep the components in the car and if possible safe (in case of accidents).

## Fouth session (06/02/2023):

We have done a lot of documentation research in relation to the problems encountered. we also thought about the structure of the code that we are going to do according to the technologies that we have chosen.

## Fifth session (20/02/2023) :

This session, with the return from vacation, we decided to refocus on the heart of the project, which is the centimeter guidance of an autonomous car. So while we were thinking about the important aspects to expose for the mid-project presentation, we started to code the retrieval of coordinates via the antenna for the car as well as the basic movements that the car executes in order to have basis for the continuation of the project. We ran into a problem, it's the voltage, the RPI is not strong enough (3V) to send signals over the full range of the car (6V).

## Sixth session (27/02/2023) :

This session we continued the preparation of the mid-project presentation. We also managed to operate the gps antenna, allowing us to retrieve our coordinates. On the other hand, we continue the code on the car side by thinking about using an Arduino to compensate for the lack of voltage.

## Seventh week (06/03/2023) :

This week we tried to use a pololu board to replace our arduino. After Monday's session, we determined that our arduino would be a better solution and therefore decided to abandon the idea of ​​using the pololu. Then this week we will continue the code part. We are in the process of making the gps output a data format that can be processed easily (if possible long/lat). Also we started coding the steering program of the car and think about the different aspects of the implementation, which functions would be useful or not. How to communicate the RPI and the gps, hence the issue of the data format. In addition we are thinking about how to save a route in the program so that the car follows it. Finally we think in parallel about the best way to mount all this on the car, taking into account the probability that the car turns over and therefore seeking to protect the equipment on board the car.

## Week Eight (13/03/2023)

This week we have and will make good progress. We got a metal plate for the gps operation. We tested it on the car and it works. On this side we still have to make sure to send readable data to the RPI to be processed. Also the code concerning the guidance of the car is progressing well, even if it was delayed by some technical problems (sd card burned). Also we are starting to look at the installation we will do on the car. We started to make the boxes to protect the hardware (Arduino and RPI) and the wiring and positioning of the latter.

## Week Nine (20/03/2023)

This week we started the last straight line. We are finalizing the installation of the hardware on the car. We are finishing the last tests on the code in order to be able to do a real life test as soon as possible. We obviously have many problems, both in the code (integration problem of some parts) and in the hardware (the RPI had a problem with the OS for some days). We are doing our best to finish in time.

## Week Ten (27/03/2024)

This week we are finishing the project. However we are facing problems. First, the charge pack for the Raspberry seems to not working, making real test hard to try. Moreover, we have difficulties to make things work on the RPI even if everything work on our computers. Those problems may need time. We will do our best to make things right.